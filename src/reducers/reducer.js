import {FETCH_POKEMONS, FETCH_ITEM} from '../actions/types';

const initialState = {
    pokemons: [],
    next: null,
    previous: null,
    count: null,
    page: null,

    name: null,
    img: null,
    weight: null,
    height: null,
    stats: [],
    render: false
};

export default function(state = initialState, action){
    switch(action.type){
        case FETCH_POKEMONS:
            return {
                ...state,
                pokemons: action.pokemons,
                previous: action.previous,
                next: action.next,
                count: action.count,
                page: action.page
            };
            break;
        case FETCH_ITEM:
            return {
                ...state,
                name: action.name,
                img: action.img,
                weight: action.weight,
                height: action.height,
                stats: action.stats,
                render: action.render
            };
            break;
        default:
            return state;
    }
}