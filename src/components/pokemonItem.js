import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showPokemon } from '../actions/action';

class PokemonItem extends Component {

    render(){
        if(this.props.render) {
            return(

                <div className="Info">
                    <img src={this.props.img} className="Image" alt="img"/>
                    <div id="title">{this.props.name.toUpperCase()}</div>
                    <div id="parameters">
                        <table>
                            <tbody>
                            <tr><td>Height:</td><td><b>{this.props.height}</b></td></tr>
                            <tr><td>Weight:</td><td><b>{this.props.weight}</b></td></tr>
                            </tbody>
                        </table>
                        <br/>
                        <div>Stats:</div>
                        <br/>
                        <div>
                            {this.props.stats.map((item) =>
                                <div key={item.stat.name}>
                                    {this.firstLetterToUpperCase(item.stat.name)}:&nbsp;
                                    <b>{item.base_stat}</b>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            )
        }

        return null;
    }

    firstLetterToUpperCase(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

const mapStateToProps = state => ({
    name: state.pokemons.name,
    img: state.pokemons.img,
    weight: state.pokemons.weight,
    height: state.pokemons.height,
    stats: state.pokemons.stats,
    render: state.pokemons.render
});

export default connect(mapStateToProps, { showPokemon })(PokemonItem);