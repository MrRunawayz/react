import React, { Component } from 'react';
import {connect} from 'react-redux';
import {fetchList} from "../actions/action";
import {showPokemon} from "../actions/action";


class Pokemons extends Component {

    componentWillMount(){
        this.props.fetchList();
    }

    switchPage(url) {
        if(url != null){
            this.props.fetchList(url);
        }
    }

    render() {

        const secondColumnStart = Math.floor(this.props.pokemons.length / 2);
        const totalPages = Math.ceil(this.props.count/20);

        return (
            <div className="page">
                <div className="List">
                    <div className="Container">

                        {this.props.pokemons.slice(0,secondColumnStart).map((item) =>{
                            return (
                                <div className="PokemonList" key={item.name}>
                                    <b><a onClick={() => this.props.showPokemon(item.url)}>{item.name.toUpperCase()}</a></b>
                                </div>
                            )}
                        )}

                        {this.props.pokemons.slice(secondColumnStart).map((item) =>
                            <div className="PokemonList" key={item.name}>
                                <b><a onClick={() => this.props.showPokemon(item.url)}>{item.name.toUpperCase()}</a></b>
                            </div>
                        )}
                        <br/>
                        <br/>
                    </div>
                    <div className="Navigation">
                        <div className="Arrow"><a onClick={() => this.switchPage(this.props.previous)}>&lt;</a></div>
                        <span>{this.props.page} of {totalPages}</span>
                        <div className="Arrow"><a onClick={() => this.switchPage(this.props.next)}>&gt;</a></div>
                    </div>
                </div>


            </div>
        );
    }

    firstLetterToUpperCase(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

const mapStateToProps = state => ({
    pokemons: state.pokemons.pokemons,
    next: state.pokemons.next,
    previous: state.pokemons.previous,
    count: state.pokemons.count,
    page: state.pokemons.page
});


export default connect(mapStateToProps, {fetchList, showPokemon})(Pokemons);
