import {FETCH_POKEMONS, FETCH_ITEM} from './types';

export const fetchList = (url) => dispatch =>{
    if(url == null) {
        url = 'https://pokeapi.co/api/v2/pokemon'
    }

    fetch(url)
        .then((response) => response.json())
        .then((responseJson) => dispatch({
            type: FETCH_POKEMONS,
            pokemons: responseJson.results,
            previous: responseJson.previous,
            next: responseJson.next,
            count: responseJson.count,
            page: countCurrentPage(url)
        }));
};

export const showPokemon = (url) => dispatch => {
    fetch(url)
        .then((response) => response.json())
        .then((responseJson) => dispatch({
            type: FETCH_ITEM,
            name: responseJson.name,
            img: responseJson.sprites.front_default,
            weight: responseJson.weight,
            height: responseJson.height,
            stats: responseJson.stats,
            render: responseJson.name == null ? false : true
        }));
}

function countCurrentPage(urlString) {
    if(urlString != null){
        const url = new URL(urlString);
        const limit = url.searchParams.get("limit");
        const offset = url.searchParams.get("offset");
        if(limit != null && offset != null){
            return offset / limit + 1;
        }
    }
    return 1;
}