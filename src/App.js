import React, { Component } from 'react';
import './App.css';
import {Provider} from 'react-redux';

import Pokemons from './components/pokemons'
import PokemonItem from './components/pokemonItem';

import store from './store.js';

class App extends Component {
    render() {
        return(
            <Provider store={store}>
                <div className="App">

                    <Pokemons />

                    <PokemonItem/>

                </div>
            </Provider>
    );
    }


}

export default App;

